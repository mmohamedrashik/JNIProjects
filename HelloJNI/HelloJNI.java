public class HelloJNI {
   static {
      System.loadLibrary("hellojni"); // Load native library at runtime
                                   // hello.dll (Windows) or libhello.so (Unixes)
   }
 
   // Declare a native method sayHello() that receives nothing and returns void
   private native void PrintME();
   private native void PrintMEE();
 
   // Test Driver
   public static void main(String[] args) {
      new HelloJNI().PrintME();  // invoke the native 
	  new HelloJNI().PrintMEE();
   }
}