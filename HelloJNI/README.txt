TSutorial URL 
https://www3.ntu.edu.sg/home/ehchua/programming/java/JavaNativeInterface.html

STEPS
1. Install MinGW
2. Java Program - > javac HelloJNI.java
3. COMPILE PROCESS

	> set JAVA_HOME=C:\Program Files\Java\jdk1.7.0_{xx}
      // Define and Set environment variable JAVA_HOME to JDK installed directory
      // I recommend that you set JAVA_HOME permanently, via "Control Panel" ⇒ "System" ⇒ "Environment Variables"
	> echo %JAVA_HOME%
      // In Windows, you can refer a environment variable by adding % prefix and suffix 
	> gcc -Wl,--add-stdcall-alias -I"%JAVA_HOME%\include" -I"%JAVA_HOME%\include\win32" -shared -o hello.dll HelloJNI.c
	
4. CREATE C File -> compille with -> 
	// Compile-only with -c flag. Output is HElloJNI.o
		> gcc -c -I"%JAVA_HOME%\include" -I"%JAVA_HOME%\include\win32" HelloJNI.c
 
	// Link into shared library "hello.dll"
		> gcc -Wl,--add-stdcall-alias -shared -o hello.dll HelloJNI.o