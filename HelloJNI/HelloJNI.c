#include <jni.h>
#include <stdio.h>
#include "HelloJNI.h"
 
// Implementation of native method sayHello() of HelloJNI class
JNIEXPORT void JNICALL Java_HelloJNI_PrintME(JNIEnv *env, jobject thisObj) {
   printf("Hello World!\n");
   return;
}
JNIEXPORT void JNICALL Java_HelloJNI_PrintMEE(JNIEnv *envs, jobject thisObjs) {
   printf("Hello World 2!\n");
   return;   
}