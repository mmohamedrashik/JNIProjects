class ArgumentPass
{
	static
	{
		  System.loadLibrary("argument");
	}
	private native String Shows(String name);
	private native int Cals(int x,int y);
	public static void main(String[] args)
	{
		String s   = new ArgumentPass().Shows("RAZ");
		System.out.println(s);
		int result =new ArgumentPass().Cals(5,10);
		System.out.println(result);
	}
}