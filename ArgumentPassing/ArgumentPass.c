#include <jni.h>
#include <stdio.h>
#include "ArgumentPass.h"
 
// Implementation of native method sayHello() of HelloJNI class
JNIEXPORT jstring JNICALL Java_ArgumentPass_Shows(JNIEnv *env, jobject obj, jstring str) {
   //printf("Hello World!\n");
   const char *inCStr = (*env)->GetStringUTFChars(env, str, NULL);
   return (*env)->NewStringUTF(env, inCStr);
}
JNIEXPORT jint JNICALL Java_ArgumentPass_Cals(JNIEnv *env, jobject obj, jint x, jint y) {
   //printf("Hello World 2!\n");
   int c = x + y;
   return c;   
}